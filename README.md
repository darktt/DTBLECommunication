# DTBLECommunication
這是我研究 app 透過藍牙進行溝通的測試程式，分為中心與周邊兩個部分，<br/>
因為這兩部分的設定方式完全不相同，因此將注意事項與說明皆寫在以下的檔案中：<br/>

* [藍牙中心（Central）](https://gitlab.com/darktt/DTBLECommunication/blob/master/DTBLECommunication/Central/README.md)
* [藍牙周邊（Peripheral）](https://gitlab.com/darktt/DTBLECommunication/blob/master/DTBLECommunication/Peripheral/README.md)
